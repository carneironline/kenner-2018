const gulp     = require('gulp')
const gulpIf   = require('gulp-if')
const imageMin = require('gulp-imagemin')
const connect  = require('gulp-connect')

module.exports = (args, src, dist) => {

    /**
     * Copia/minifica imagens
     */
    gulp.task('images', () => {
        return gulp.src(src + 'assets/img/*')
        .pipe(gulpIf(args.production, imageMin()))
        .pipe(gulp.dest(dist + 'assets/img'))
    })

    /**
     * Copia fontes
     */
    gulp.task('fonts', () => {
        return gulp.src(src + 'assets/fonts/*')
        .pipe(gulp.dest(dist + 'assets/fonts'))
    })

    /**
     * Copia HTML
     */
    gulp.task('html', () => {
        return gulp.src(src + '*.html')
        .pipe(gulp.dest(dist))
        .pipe(connect.reload())
    })

    /**
     * HTML Watch
     */
    gulp.task('watchHTML', () => {
        return gulp.watch(src + '*.html', ['html'])
        .on('change', (event) => {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...')
        })
    })
}
