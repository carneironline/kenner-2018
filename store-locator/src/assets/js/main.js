
import Vue from 'vue'
import $ from 'jquery'
import jQuery from 'jquery'

import Data from './modules/data'
import Methods from './modules/methods'
import System from './modules/system'

window.$ = $
window.jQuery = jQuery

new Vue ({
    el: 'body',
    data: Data,
    methods: Methods,
    ready: System.ready,
})
