
import Gmaps from './gmaps'

import swal from 'sweetalert'

export default {
    ready () {
        let self = this

        $.ajax({
            type: 'get',
            headers: self.request.headers,
            url: self.request.url
        }).then( (response) => {
            self.addresses = response

            for ( let i = 0; i < self.addresses.length; i++ ) {
                self.addresses[i].lat = parseFloat(self.addresses[i].lat)
                self.addresses[i].lng = parseFloat(self.addresses[i].lng)
            }

            self.init()
        }, (response) => {
            if ( response.statusText == "error" ) {
                swal('Por favor, atualize sua página', '', 'error')
            }
        })

        $.ajax({
            url: self.request.geoLocationUrl + self.request.apiKeyGeoLocation,
            type: 'post'
        }).then( (response) => {
            self.userLocation = response.location
        })

        Gmaps.infowindow = new google.maps.InfoWindow()
        Gmaps.directionsDisplay = new google.maps.DirectionsRenderer()
    }
}
