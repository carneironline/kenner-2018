
import Data from './data'
import Gmaps from './gmaps'
import Helpers from '../utils/helpers'

import { chunk, findIndex, minBy } from 'lodash'
import swal from 'sweetalert'

export default {
    init () {
        this._setSelectFields()
        this._getField()
        this._closestStore()
        this._getAllStores()
    },

    _getAllStores () {
        let self = this;
        let a = self.addresses;

        for ( let i = 0, len = a.length; i < len; i += 1 ) {
            self._storeFields(i);
        }

        Gmaps.initMap();
    },

    _initLocation () {
        Data.distance.value = 9999999999
        Data.distance.index = ''

        let self = this
        let origin = `${Data.userLocation.lat},${Data.userLocation.lng}`
        let destination = {}
        let addressIndex = ''
        let destinations = []
        let arrIndex = 1
        let service = new google.maps.DistanceMatrixService;
        let mapProp = {
            scrollwheel: true,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        let map = new google.maps.Map(document.getElementById("x-map"), mapProp)

        for ( let i = 0; i < Data.addresses.length; i++ ) {
            destinations.push({lat: Data.addresses[i].lat, lng: Data.addresses[i].lng})
        }

        destinations = chunk(destinations, 25)
        destinations = Helpers.shuffleArray(destinations)

        for ( let i = 0; i < destinations.length; i++ ) {
            service.getDistanceMatrix({
                origins: [Data.userLocation],
                destinations: destinations[i],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, (response, status) => {
                if ( status === 'OK' ) {
                    let el = response.rows[0].elements
                    let min = minBy(el, (obj) => {
                        if ( obj.status === 'OK') {
                            return obj.distance.value
                        }
                    })

                    let minDist = min.distance.value
                    let minTime = min.duration.value
                    let formattedMinDist = min.distance.text
                    let formattedMinTime = min.duration.text

                    let index = findIndex(el, (obj) => {
                        if ( obj.status != 'NOT_FOUND' && obj.status != 'ZERO_RESULTS' ) {
                            return obj.distance.value == minDist
                        }
                    })

                    if ( minDist < Data.distance.value ) {
                        Data.distance.value = minDist
                        destination = destinations[i][index]
                        addressIndex = findIndex(Data.addresses, (obj) => {
                            return obj.lat == destination.lat
                        })
                    }

                    if ( i === (destinations.length - 1) ) {
                        self._storeFields(addressIndex)
                        Gmaps.getClosestPosition(map, origin, destination)
                    }
                } else if ( status === 'OVER_QUERY_LIMIT' ) {
                    swal('Oppsss!', 'Por favor, aguarde um momento antes de realizar uma nova busca ou atualize a página', 'error')
                } else {
                    swal('Não exitem rotas cadastradas', '', 'error')
                }
            })
        }
    },

    _closestStore () {
        let $link = $('.x-main-body__link')

        $link.on('click', (ev) => {
            let $this = $(ev.currentTarget)
            ev.preventDefault()

            if ( ! $this.hasClass('block') ) {
                $this.addClass('block')
                this._clearFields()
                this._initLocation()
            }
        })
    },

    _getField () {
        let self = this

        $('form[name="search-form"]').on('submit', (ev) => {
            ev.preventDefault()

            let $selectedState = $(`#state option:selected`).val()
            let $selectedCity = $(`#city option:selected`).val()

            if ( !!!$selectedState ) {
                swal('Por favor, escolha um Estado', '', 'error')
                return
            }

            $('.x-main-body__link').removeClass('block')

            self._getStores({
                cidade: $selectedCity,
                estado: $selectedState
            })
        })
    },

    _getStores (obj) {
        let foundedAddress = false
        this._clearFields()

        if ( this._getStoreLocation(this.addresses, obj.cidade, 'cidade') ) {
            Gmaps.initMap()
            foundedAddress = true
            return
        }

        if ( this._getStoreLocation(this.addresses, obj.estado, 'estado') ) {
            Gmaps.initMap()
            foundedAddress = true
            return
        }

        if ( ! foundedAddress ) {
            swal('Nenhuma loja encontrada', '', 'error')
        }
    },

    _getStoreLocation (storeAddresses, inputAddress, locate) {
        let self = this
        let j = 0
        let foundedStore = false

        for ( let i = 0; i < storeAddresses.length; i++ ) {
            let address = storeAddresses[i]

            if ( address[locate] !== null && inputAddress !== null ) {
                if ( Helpers.removeAccent(address[locate].toLowerCase()) === Helpers.removeAccent(inputAddress.toLowerCase()) ) {
                    foundedStore = true
                    self._storeFields(i)
                }
            }
        }

        return foundedStore
    },

    _storeFields (index) {
        let i = Data.addresses[index]
        let id = i.id
        let name = (i.nome !== null) ? i.nome : ''
        let address = (i.endereco !== null) ? i.endereco : ''
        let addressNumber = (i.numero !== null) ? i.numero : ''
        let complement = (i.complemento !== null) ? i.complemento : ''
        let district = (i.bairro !== null) ? i.bairro : ''
        let city = (i.cidade !== null) ? i.cidade : ''
        let state = (i.estado !== null) ? i.estado : ''
        let zipCode = (i.cep !== null) ? i.cep : ''
        let email = (i.email !== null) ? i.email : ''
        let phone = (i.telefone !== null) ? i.telefone : ''
        let cel = (i.celular !== null) ? i.celular : ''
        let lat = (i.lat !== null) ? i.lat : ''
        let lng = (i.lng !== null) ? i.lng : ''
        let nameImage = this.slugify(name);

        // Prepare fields
        let completeAddress = `${address}, ${addressNumber}`
        let completeState = `${district} - ${city} - ${state}`
        let fullAddress = `${address}, ${addressNumber}, ${zipCode}, ${state}`

        completeAddress = (complement === '') ? completeAddress : `${completeAddress} - ${complement}`

        let elements = `
            <li class="x-main-body__results--item">
                <div class="x-main-body__results-wrapper">
                    <h4 class="x-main-body__results--name">${name}</h4>
                    <div class="x-main-body__results--address">
                        <p class="x-main-body__results--complete-address">${completeAddress}</p>
                        <p class="x-main-body__results--complete-state">${completeState}</p>
                    </div>

                    <p class="x-main-body__results--contato">Contato</p>
                    <p class="x-main-body__results--phone">${phone}</p>
                    <p class="x-main-body__results--email">${email}</p>
                    <p class="x-main-body__results--id x-main-body__results--hidden">${id}</p>
                    <p class="x-main-body__results--full-address x-main-body__results--hidden">${fullAddress}</p>
                    <p class="x-main-body__results--lat x-main-body__results--hidden">${lat}</p>
                    <p class="x-main-body__results--lng x-main-body__results--hidden">${lng}</p>
                </div>
            </li>`

        $('.x-main-body__results--items').append(elements)
    },

    _selectFields (selectName, value) {
        const a = Data.addresses
        let s = []

        $(`#${selectName}`).empty().append(`<option value="" selected="selected" disabled="disabled">${value.toUpperCase()}</option>`)

        for ( let i = 0; i < a.length; i++ ) {
            s.push(a[i][`${value}`])
        }

        s = Helpers.cleanArray(s)
        s = s.map(Helpers.removeAccent)
        s = Helpers.unique(s.map( (x) => x.toUpperCase())).sort()

        for ( let i = 0; i < s.length; i++ ) {
            $(`#${selectName}`).append(`<option value="${s[i]}">${s[i]}</option>`)
        }

        s = []
    },

    _setSelectFields () {
        this._selectFields('state', 'estado')
        this._completeFields({state: 'state', city: 'city'}, 'cidade')
    },

    _completeFields (selectObj, value) {
        $(`#${selectObj.state}`).on('change', (ev) => {
            let $this = $(ev.currentTarget)
            let fieldSelect = []
            let a = Data.addresses

            if ( $this.val() === '' ) {
                $(`#${selectObj.city}`).empty().append(`<option value="">${value.toUpperCase()}</option>`).hide()
            } else {
                $(`#${selectObj.city}`).show()
                $('#x-submit').show()
            }

            for ( let i = 0; i < a.length; i++ ) {
                if ( a[i].estado !== null ) {
                    if ( a[i].estado.toLowerCase() === $this.val().toLowerCase() ) {
                        fieldSelect.push(a[i][`${value}`])
                    }
                }
            }

            fieldSelect = fieldSelect.map(Helpers.removeAccent)
            fieldSelect = Helpers.unique(fieldSelect.map( (x) => x.toUpperCase())).sort()

            $(`#${selectObj.city}`).empty().append(`<option value="">${value.toUpperCase()}</option>`)

            for ( let i = 0; i < fieldSelect.length; i++ ) {
                $(`#${selectObj.city}`).append(`<option value="${fieldSelect[i]}">${fieldSelect[i]}</option>`)
            }
        })
    },

    _clearFields () {
        $('.x-main-body__results--items').empty()
        $('#x-map').empty()
    },

    slugify (text) {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    },
}
