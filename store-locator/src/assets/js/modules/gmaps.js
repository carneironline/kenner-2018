
import Data from './data'

export default {
    data: {
        markers: [],
        userLocation: {},
        infowindow: {},
        directionsDisplay: {},
    },

    getClosestPosition (map, origin, destination) {
        let iconMap = Data.markers.store

        this._calculateAndDisplayRoute(map, origin, destination)
        this._changeAddressMap(map)
    },

    initMap () {
        let mapOptions = {
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        let map = new google.maps.Map(document.getElementById("x-map"), mapOptions);
        let iconMap = Data.markers.store
        let $items = $('.x-main-body__results--items').children('li')
        let latlng = []

        for ( let i= 0; i < $items.length; i++ ) {
            latlng.push({
                lat: parseFloat($($items[i]).find('.x-main-body__results--lat').text()),
                lng: parseFloat($($items[i]).find('.x-main-body__results--lng').text())
            })
        }

        this._allResultsMarkers(map, latlng, iconMap)
        this._changeAddressMap(map)
    },

    _changeAddressMap (map) {
        let iconMap = Data.markers.store
        let self = this

        $('.x-main-body__results').on('click', '.x-main-body__results--item', (ev) => {
            let $this = $(ev.currentTarget)
            let latlng = {
                lat: parseFloat($this.find('.x-main-body__results--lat').text()),
                lng: parseFloat($this.find('.x-main-body__results--lng').text())
            }

            $this.addClass('active')
            $this.siblings().removeClass('active')

            self._deleteMarkers()

            map.setCenter(latlng)
            map.setZoom(16)

            let marker = new google.maps.Marker({
                map: map,
                position: latlng,
                icon: iconMap
            })

            this._fixMarkerSelected(map, marker)
        })
    },

    _allResultsMarkers (map, locations, icon) {
        let self = this
        let markerBounds = new google.maps.LatLngBounds()

        for ( let i = 0; i < locations.length; i++ ) {
            let location = locations[i]
            let latlng = new google.maps.LatLng(location.lat, location.lng)

            let marker = new google.maps.Marker({
                map: map,
                position: latlng,
                icon: icon,
                animation: google.maps.Animation.DROP
            })

            markerBounds.extend(location)
            map.fitBounds(markerBounds)

            self.data.markers.push(marker)
        }
    },

    _fixMarkerSelected (map, marker) {
        let self = this
        let $item = $('.x-main-body__results--item.active')
        let name = $item.find('.x-main-body__results--name').text()
        let completeAddress = $item.find('.x-main-body__results--complete-address').text()
        let completeState = $item.find('.x-main-body__results--complete-state').text()
        let phone = $item.find('.x-main-body__results--phone').text()
        let email = $item.find('.x-main-body__results--email').text()
        let content = `
                <div class="x-main-body__map--info">
                    <h4 class="x-main-body__results--name">${name}</h4>
                    <div class="x-main-body__results--address">
                        <p class="x-main-body__results--complete-address">${completeAddress}</p>
                        <p class="x-main-body__results--complete-state">${completeState}</p>
                    </div>

                    <p class="x-main-body__results--contato">Contato</p>
                    <p class="x-main-body__results--phone">${phone}</p>
                    <p class="x-main-body__results--email">${email}</p>
                </div>`

        marker.addListener('click', () => {
            self.infowindow.setContent(content)
            self.infowindow.open(map, marker)
        });

        self.data.markers.push(marker)
    },

    _calculateAndDisplayRoute (map, origin, destination) {
        let self = this
        let infoWindow = new google.maps.InfoWindow();
        let rendererOptions = {
            polylineOptions: {
                strokeColor: "#2c3b4a",
                strokeOpacity: 0.5,
                strokeWeight: 6
            },
            suppressMarkers : true,
            InfoWindow: infoWindow
        }
        let icons = {
            start: new google.maps.MarkerImage(Data.markers.user),
            end: new google.maps.MarkerImage(Data.markers.store)
        }
        let directionsService = new google.maps.DirectionsService;

        if ( self.directionsDisplay != null ) {
            self.directionsDisplay.setMap(null);
            self.directionsDisplay = null;
        }

        self.directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions)
        self.directionsDisplay.setMap(map)

        directionsService.route({
            origin: origin,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        }, (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                self.directionsDisplay.setDirections(response)

                let leg = response.routes[0].legs[0]
                self._makeMarker(map, leg.start_location, icons.start)
                self._makeMarker(map, leg.end_location, icons.end)
            } else {
                window.console.log('Directions request failed due to ' + status)
            }
        })
    },

    _makeMarker (map, position, icon) {
        let self = this
        let marker = new google.maps.Marker({
            map: map,
            position: position,
            icon: icon
        })

        // marker.addListener('click', () => {
        //     self.infowindow.setContent('<h4>VOCÊ ESTÁ AQUI!</h4>')
        //     self.infowindow.open(map, marker)
        // })
    },

    _deleteMarkers () {
        let markers = this.data.markers

        for ( let i = 0; i < markers.length; i++ ) {
            markers[i].setMap(null)
        }

        markers = []
    },
}
