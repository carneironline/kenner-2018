
export default {
    addresses: [],
    request: {
        headers: {
            'Accept': 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json',
            'REST-Range': 'resources=0-500',
        },
        url: 'https://api.vtexcrm.com.br/kenner/dataentities/SL/search?_fields=id,nome,endereco,numero,complemento,bairro,cidade,estado,cep,email,telefone,celular,lat,lng',
        geoLocationUrl: 'https://www.googleapis.com/geolocation/v1/geolocate?key=',
        apiKeyGeoLocation: "AIzaSyAT5Rt7w4G3SCL-NTZEm5rDlvioYPpHvh0"
    },
    userInfo: {},
    userLocation: {},
    markers: {
        store: '/arquivos/icon-user.png',
        user: '/arquivos/icon-user.png',
    },
    distance: {
        value: 9999999999,
        index: '',
    },
}
