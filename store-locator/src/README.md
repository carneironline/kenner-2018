
## Informações

* VueJS (Promisses)
* ES2015 (Babel)
* Modularização de JS com Browserify
* SASS (Modules/Components)
* Gulp
* BEM
* Flexbox (IE10+)


## Gerar os arquivos

Com o [NodeJS](https://nodejs.org/en/) (versão 6.x.x) e [Gulp](http://gulpjs.com/) devidamente instalados:

* Instale as dependências com: `$ npm install`
* Para ambiente de desenvolvimento, rode: `$ npm run dev`
    * O atalho compila todos os arquivos e sobe automaticamente um server em `http://localhost:8080`
* Para o build final, rode: `$ npm run build`
    * Concatenará e minificará todos os assets para o deploy


## Organização

Desenvolvimento é feito no diretório `/src/`

Após compilados, é gerado o diretório `/dist/`
