var Kenner = {

    data: {

        floatToCurrency : function(_float) {
            var s = _float.toString().replace(',', '').split('.'),
            decimals = s[1] || '',
            integer_array = s[0].split(''),
            formatted_array = [];

            for (var i = integer_array.length, c = 0; i != 0; i--, c++) {
                if (c % 3 == 0 && c != 0) {
                    formatted_array[formatted_array.length] = '.';
                }
                formatted_array[formatted_array.length] = integer_array[i - 1];
            }

            if (decimals.length == 1) {
                decimals = decimals + '0';
            } else if (decimals.length == 0) {
                decimals = '00';
            } else if (decimals.length > 2) {
                decimals = Math.floor(parseInt(decimals, 10) / Math.pow(10, decimals.length - 2)).toString();
            }

            return '<span>R$</span> ' + formatted_array.reverse().join('') + ',' + decimals;
        },

        cookieRead: function(_name) {
            var nameEQ = _name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        },

        cookieCreate: function(_name,_value,_days) {
            var d = new Date(); 
            d.setTime(d.getTime() + (_days*1000*60*60*24)); 
            var expires = "expires=" + d.toGMTString(); 
            window.document.cookie = _name+"="+_value+"; "+expires + ";path=/";
        }
    },

    methods: {   

        getWishlistedItems : function(){
            var _wishlisted = Kenner.data.cookieRead('wishlist');
            if( _wishlisted != undefined ){
                _wishlisted.split(',');
                var a = _wishlisted;
                var x = new Array();
                x = a.split(",");
                $('.js--wishlist-count').html(x.length);
            }else{
                $('.js--wishlist-count').html('0');
            }
        },

        login: function() {
            $('.js--login').click(function(e) {
                 
                $.ajax({
                    type: 'GET',
                    url: '/no-cache/profileSystem/getProfile',
                    success: function(data) {                        
                        
                        if (data.IsUserDefined == false) {
                      
                            e.preventDefault();
                            vtexid.start({
                                returnUrl: '/account',
                                userEmail: '',
                                locale: 'pt-BR',
                                forceReload: false
                            });

                            
                        } else {
                            window.location.href = "/account";
                        };
                    }
                });
                return false;
            });

            $('.js--wishlist').click(function(e) {
                $.ajax({
                    type: 'GET',
                    url: '/no-cache/profileSystem/getProfile',
                    success: function(data) {
                        if (data.IsUserDefined == false) {

                            e.preventDefault();
                            vtexid.start({
                                returnUrl: '/account',
                                userEmail: '',
                                locale: 'pt-BR',
                                forceReload: false
                            });
                        } else {
                            window.location.href = "/account/wishlist";
                        };
                    }
                });
                return false;
            });
        },

        removeHelpComplement : function() {
            $('.helperComplement').remove();
        },

        openNavigationMenu : function(){
            var _open = $('.js--menu-open');
            var _navigation = $('.x-header__navigation');

            _open.click(function(){
                $(this).toggleClass('is--active');
                _navigation.slideToggle();
            });
        },  

        openHeaderSearch : function(){
            $('.js--search-open').click(function(){
                $(this).hide();
                $(this).parents('li').addClass('is--open');
                setTimeout(function(){
                    $('.x-header__form').slideToggle();       
                }, 0200)
            });
        },

        headerSearch : function(){
            $('.js--header-form').submit(function(event) {
                var _therm = $(this).find('.js--form-input').val();
                if (_therm != "") {
                    if( _therm != 'a' ){
                        window.location.href = "/" + _therm;
                    }
                };
                return false;
            });
            $('.js--form-submit').click(function(){
                $('.js--header-form').submit();
            });
        },  

        openMinicart : function(){
            var _open = $('.js--minicart-open');
            var _close = $('.js--minicart-close');
            _open.click(function(){
                $(this).parents('li').toggleClass('is--active');
                $('.x-minicart').toggleClass('is--active');
            });
            _close.click(function(){
                _open.trigger('click');
            });
        },

        checkMinicartItems : function(){
            var _items = $('.portal-totalizers-ref .amount-items-em').eq(0).text();
            if( _items == '1' ){
                $( '.x-minicart__items > p' ).html(_items + ' Unidade');
            }else{
               $( '.x-minicart__items > p' ).html(_items + ' Unidades'); 
            }
            //$('.js--minicart-items').html(_items);
            $('.js--minicart-count').html(_items);
        },

        buyToMinicart : function(){
            var _button = $('.x-product .js--add-cart');
            _button.click(function(){
                var sku  = $(this).attr('data-sku');

                var product = {
                    id: sku,
                    quantity: 1,
                    seller: 1
                };

                return vtexjs.checkout.addToCart([product]).done(function(orderForm) {
                    jQuery.vtex_quick_cart(optionsMiniCart);

                    setTimeout(function(){
                        $('html, body').animate({
                            scrollTop: $("body").offset().top
                        }, 0200);
                        $('body').addClass('is--loading');
                    }, 0200);

                    setTimeout(function(){
                        $('body').removeClass('is--loading');
                    }, 1500);

                    setTimeout(function(){
                        $('.js--minicart-open').parents('li').addClass('is--active');
                        $('.x-minicart').addClass('is--active');
                    }, 1600);

                    setTimeout(function(){
                        $('.js--minicart-open').parents('li').removeClass('is--active');
                        $('.x-minicart').removeClass('is--active');
                    }, 4000);

                });
                return false;  
            })
        },

        getCurrentDate : function(){
            var _fullDate = new Date();
            var _day = _fullDate.getDay();
            var minutesTwoDigitsWithLeadingZero = ("0" + _fullDate.getMinutes()).substr(-2);

            switch(_day) {
                case 1:
                _day = "SEG"
                break;
                case 2:
                _day = "TER"
                break;
                case 3:
                _day = "QUA"
                break;
                case 4:
                _day = "QUI"
                break;
                case 5:
                _day = "SEX"
                break;
                case 6:
                _day = "SAB"
                break;
                case 0:
                _day = "DOM"
                break;    
                default:
            };

            $('.js--day').html(_day);
            $('.js--hour').html(_fullDate.getHours() + "<span>:</span>" + minutesTwoDigitsWithLeadingZero);


        },

        backTopFunction : function(){
            $('.js--back-top').click(function(){
                $('html, body').animate({
                    scrollTop: $("body").offset().top
                }, 0200);
            });
        },

        createVideoIframe : function(){
            $('.x-banners__image .box-banner a').click(function(e){
                e.preventDefault();
                var _href = $(this).attr('href'); 
                if( !_href.indexOf('youtube') != -1 ){
                    var _videoFormatted = _href.replace('watch?v=', 'embed/');
                    var _videoIframe = $('.js--iframe iframe').attr('src', _videoFormatted + '?rel=0&amp;controls=0&amp;showinfo=0');
                    $('.js--iframe').fadeIn();
                }
            });
            $('.js--iframe .x-overlay').click(function(){
                $('.js--iframe').fadeOut();
                $('.js--iframe iframe').attr('src', '');
            });
        },

        updateWishlistItems : function(){
            $( '.x-vitrine .js--add-wishlist' ).click(function(){
                setTimeout(function(){
                    Kenner.methods.getWishlistedItems();
                }, 3000);
            });
        },

        registerMinMaxLength: function(target, min, max) {
            $('body').on('keydown keyup change', target, function(){
                var char = $(this).val();
                var charLength = $(this).val().length;
                var maxLength = max | 10;
                var minLenght = min | 3;
                
                if(charLength < minLenght){
                    //console.log('minimum');
                }else if(charLength > maxLength){
                    //console.log('max');
                    $(this).val(char.substring(0, maxLength));
                }else{
                    //console.log('valid');
                }
            });
        },
        activeMinMaxLength: function() {
            Kenner.methods.registerMinMaxLength('#inputEmail', 3, 80);
            Kenner.methods.registerMinMaxLength('#appendedInputButton', 3, 80);
            Kenner.methods.registerMinMaxLength('#email', 3, 80);
            Kenner.methods.registerMinMaxLength('#firstName', 3, 50);
            Kenner.methods.registerMinMaxLength('#lastName', 3, 50);
            Kenner.methods.registerMinMaxLength('#nickName', 0, 50);
            Kenner.methods.registerMinMaxLength('#addressName', 3, 53);
            Kenner.methods.registerMinMaxLength('#receiverName', 3, 50); 
            Kenner.methods.registerMinMaxLength('#number', 1, 6);
            Kenner.methods.registerMinMaxLength('#complement', 0, 20);
            Kenner.methods.registerMinMaxLength('#reference', 0, 50);
            Kenner.methods.registerMinMaxLength('#neighborhood', 0, 40);
            Kenner.methods.registerMinMaxLength('#city', 0, 40);
        },

        init : function() {
            this.activeMinMaxLength();
            this.getWishlistedItems();
            this.login();
            this.removeHelpComplement();
            this.openMinicart();
            this.buyToMinicart();
            this.getCurrentDate();
            this.openNavigationMenu();
            this.openHeaderSearch();
            this.headerSearch();
            this.backTopFunction();
            this.createVideoIframe();
            this.updateWishlistItems();
        },

        init_ajax: function() {
            this.removeHelpComplement();
            this.checkMinicartItems();
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner.mounted();
});

$(document).ajaxStop(function () {
    Kenner.ajax();
});