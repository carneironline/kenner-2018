var Kenner_Home = {

    methods: {

        sliderBannerPrincipal : function(){ 
            var _slider = $('.x-banner .x-banner__list');
            var _options = {
                slidesToShow: 1, 
                slidesToScroll: 1,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                infinite: true,
                arrows: true,
                dots: true
            };
            _slider.slick(_options);
        },

        sliderVitrines : function(){
            var _vitrines = $('.x-vitrine-featured .x-vitrine__carousel ul');
            var _options = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                arrows: true
            };
            _vitrines.slick(_options);
        },

        slideVitrineNovidades : function(){
            var _vitrines = $('.x-news .x-vitrine__carousel ul');
            var _options = {
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: true,
                infinite: true,
                arrows: true
            };
            _vitrines.slick(_options);
        },

        getVitrineArrows : function(){
            var _slider = $('.x-news .x-vitrine__carousel ul');
            var _arrowsPrev = _slider.find('.slick-prev').html();
            var _arrowsNext = _slider.find('.slick-next').html();
            _slider.find('.slick-dots').prepend('<span class="x-slick-prev">'+_arrowsPrev+'</span>');
            _slider.find('.slick-dots').append('<span class="x-slick-next">'+_arrowsNext+'</span>');

            $('body').on('click', '.x-slick-prev', function(){
                _slider.find('.slick-prev').trigger('click');
            });
            $('body').on('click', '.x-slick-next', function(){
                _slider.find('.slick-next').trigger('click');
            });
        },

        getBannerAlt : function(){
            var _banner = $('.x-news .box-banner img').attr('alt');
            $('.x-news .box-banner').append( '<h3>'+_banner+'</h3>' );
        },

        init : function() {
            this.sliderBannerPrincipal();
            this.sliderVitrines();
            this.slideVitrineNovidades();
            this.getVitrineArrows();
            this.getBannerAlt();
        },

        init_ajax: function() {

        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Home.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Home.ajax();
});