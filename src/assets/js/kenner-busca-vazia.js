var Kenner_Busca_Vazia = {

    methods: {

        search: function () {
            $('.x-search-bar form').submit(function (e) {

                var searchText = $('#textSearch').val();

                if(searchText != ''){
                    window.location.href = '/'+searchText;

                }

                return false;
            });
        },

        init: function () {
            this.search();
        },

        init_ajax: function () {

        },
    },

    ajax: function () {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Busca_Vazia.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Busca_Vazia.ajax();
});