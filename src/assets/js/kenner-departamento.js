var Kenner_Departamento = {

    methods: {  

        mainBannerSlider : function(){
            var _slider = $('.x-departamento-top__banner');
            var _options = {
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                dots: true,
                arrows: false,
            }
            _slider.slick(_options);
        },

        openOrderBy : function(){
            $('.js--orderby-open').click(function(){
                $(this).toggleClass('is--active');
                $('.js--orderby').slideToggle();
            });
        },

        getOrderBy : function(){
            $('.js--orderby a').on('click', function(event) {
                var url = window.location.href;
                var rgxp1 = /PS=\d+/g;
                var rgxp2 = /O=.+/g;
                var finalURL;
                var search = $(this).attr('href');

                var path = window.location.pathname.replace(/de-\d+-a-\d+/, '');

                finalURL = window.location.origin + path + '?PS=12' + search;

                window.location.href = finalURL; 

                return false;
            });

            var _orderActive = window.location.search.split('O=');
            if (_orderActive.length > 1) {
                _orderActive = _orderActive[1];

                $('.js--orderby a').each(function(index, el) {
                    var _element = $(this);
                    var _value = _element.attr('href').replace('&O=', '');

                    if (_orderActive == _value) {
                        var _text = _element.text();
                        $('.js--orderby-open').text(_text);
                    };
                });
            };
        },

        getProductsLength : function(){
            var _value = $('.searchResultsTime .resultado-busca-numero .value').eq(0).text();
            $('.js--products-items').html(_value + ' produtos');
        },

        checkBanners : function(){
            setTimeout(function(){
                if(!$('.x-departamento-top__banner .slick-list li img').length){
                    $('.x-departamento-top').addClass('has--no-banner');
                }
            }, 0500);
        },

        init : function() {
            this.mainBannerSlider();
            this.openOrderBy();
            this.getProductsLength();
            this.getOrderBy();
            this.checkBanners();
        },

        init_ajax: function() {
            this.getProductsLength();
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Departamento.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Departamento.ajax();
});

$(window).load(function(){
    $('.x-departamento-content__filters').addClass('is--loaded');
});