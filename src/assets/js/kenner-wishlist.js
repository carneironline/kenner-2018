var _headers = { 'Accept': 'application/vnd.vtex.ds.v10+json', 'Content-Type': 'application/json' };
var _storeName = 'kenner';

var wishlist = {

    data: {

        cookieRead: function(_name) {
            var nameEQ = _name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        },

        cookieCreate: function(_name,_value,_days) {
            var d = new Date(); 
            d.setTime(d.getTime() + (_days*1000*60*60*24)); 
            var expires = "expires=" + d.toGMTString(); 
            window.document.cookie = _name+"="+_value+"; "+expires + ";path=/";
        }

    },

    methods: {

        login: function() {
            vtexid.start({
                     returnUrl: '',
                     userEmail: '',
                     locale: 'pt-BR',
                     forceReload: false
            });
        },  

        click: function() {
            var _product = '.x-wishlist-button';
            var _shelf = '.js--add-wishlist';

            $(_product).click(function(event) {
                var _element = $(this);

                if (!_element.hasClass('x-selected')) {
                    var _product_id = skuJson_0.productId;
                    var _cookie_ids_list = wishlist.data.cookieRead('wishlist');

                    if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                        _cookie_ids_list = _cookie_ids_list + ',' + _product_id;
                    } else {
                        _cookie_ids_list = _product_id;    
                    };

                    wishlist.methods.user(_cookie_ids_list);
                };
            }); 

            $(_shelf).click(function(event) {
                var _element = $(this);

                if (!_element.hasClass('x-selected')) {
                    _element.addClass('x-selected');
                    var _product_id = _element.closest('.x-product').find('.x-id').val();
                    var _cookie_ids_list = wishlist.data.cookieRead('wishlist');

                    if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                        _cookie_ids_list = _cookie_ids_list + ',' + _product_id;
                    } else {
                        _cookie_ids_list = _product_id;    
                    };

                    wishlist.methods.user(_cookie_ids_list);
                };
            });
        },

        cookie: function(_cookie_ids_list) {
            wishlist.data.cookieCreate('wishlist', _cookie_ids_list, 20);
            wishlist.methods.success();
        },

        user: function(_cookie_ids_list) {
            vtexjs.checkout.getOrderForm().done(function(_user_infos){
                var _email = _user_infos.clientProfileData.email;

                if (_email == "" || _email == null) {
                    wishlist.methods.login();
                } else {
                    wishlist.methods.userid(_email, _cookie_ids_list);
                };

            });
        },

        userid: function(_email, _cookie_ids_list) {
            $.ajax({
                headers: _headers,
                type: 'GET',
                url: 'https://api.vtexcrm.com.br/'+_storeName+'/dataentities/CL/search?_where=email='+_email+'&_fields=id',

                success: function(data) {
                    if( $(data).length ){
                        var _user_id = data[0].id;
                        wishlist.methods.get(_user_id, _cookie_ids_list);
                    }else{
                        wishlist.methods.createUser(_email);   
                    }
                }
            });
        },

        get: function(_user_id, _cookie_ids_list) {
            $.ajax({
                headers: _headers,
                type: 'GET',
                url: 'https://api.vtexcrm.com.br/'+_storeName+'/dataentities/WL/search?_where=userId=' + _user_id + '&_fields=id,productsList',

                success: function(_wishlist) {
                    if (_wishlist.length) {
                        var _id_masterdata = _wishlist[0].id;
                        var _check_cookie = wishlist.data.cookieRead('wishlist');

                        if (_check_cookie == "" || _check_cookie == null) { 
                            _cookie_ids_list = _cookie_ids_list + _wishlist[0].productsList;
                        };
                        
                        wishlist.methods.update(_id_masterdata, _user_id, _cookie_ids_list);
                    } else {

                        if (wishlist.data.cookieRead('id-lista') != "" && wishlist.data.cookieRead('id-lista') != null) {
                            _id_masterdata = wishlist.data.cookieRead('id-lista');
                            wishlist.methods.update(_id_masterdata, _user_id, _cookie_ids_list);
                        } else {
                            wishlist.methods.create(_user_id, _cookie_ids_list);
                        };
                    };

                }
            });
        },

        create: function(_user_id, _cookie_ids_list) {

            var _json = {
                "userId": _user_id,
                "productsList": _cookie_ids_list
            };

            $.ajax({
                headers: _headers,
                data: JSON.stringify(_json),
                type: 'PATCH',
                url: 'https://api.vtexcrm.com.br/'+_storeName+'/dataentities/WL/documents/',

                success: function(_wishlist) {
                    var _id_masterdata = _wishlist.DocumentId;
                    wishlist.data.cookieCreate('id-lista', _id_masterdata, 20);
                    wishlist.methods.cookie(_cookie_ids_list);
                }
            });
        },

        update: function(_id_masterdata, _user_id, _cookie_ids_list) {

            var _json = {
                "userId": _user_id,
                "productsList": _cookie_ids_list
            };

            $.ajax({
                headers: _headers,
                data: JSON.stringify(_json),
                type: 'PATCH',
                url: 'https://api.vtexcrm.com.br/'+_storeName+'/dataentities/WL/documents/' + _id_masterdata,

                success: function(_wishlist) {
                    wishlist.methods.cookie(_cookie_ids_list);
                }
            });
        },

        success: function() {
            wishlist.methods.seleteds();
        },

        seleteds: function() {
            var _cookie_ids_list = wishlist.data.cookieRead('wishlist');
            if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                var _products = _cookie_ids_list.split(',');

                if ($('body').hasClass('produto')) {
                    var _button = $('.x-product__infos--buy-call-to-action .x-add-to-wishlist');

                    for (var i = _products.length - 1; i >= 0; i--) {
                        var _product_id = _products[i];

                        if (_product_id == skuJson_0.productId) {
                            _button.addClass('x-selected');
                        };
                    };
                };

                $('.x-vitrine .x-product').each(function(index, el) {
                    var _element = $(this);
                    var _id = _element.find('.x-id').val();

                    for (var i = _products.length - 1; i >= 0; i--) {
                        var _product_id = _products[i];

                        if (_product_id == _id) {
                            _element.find('.x-wishlist').addClass('x-selected');
                        };
                    };

                });    

            }; 
        },

        page: function() {
            if ($('body').hasClass('wishlist')) {
                var _cookie_ids_list = wishlist.data.cookieRead('wishlist');
                if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                    var _product_id = _cookie_ids_list.split(/,/g);

                    var _list_products = _product_id.filter(function(id){
                        return $.trim(id);
                    }).map(function(id){
                       return 'fq=productId:'+id
                   }).join('&');

                    var _shelf_id = '1ae9439e-04fa-4ec1-975e-31082d2b3a4e';
                    $.ajax('/buscapagina?'+_list_products+'&PS=40&sl='+_shelf_id+'&cc=100&sm=0&PageNumber=1', {async:false})
                    .done(function(data){
                        $('#x-get-products').html(data);
                    }).fail(function(){
                        console.log('Ocorreu um erro!');
                    });

                } else {
                    $('#x-get-products').html("Você ainda não adicionou produtos em sua wishlist!");
                };
            };
        },

        createUser : function(_email){
            var _json = {
                "email": _email
            };

            $.ajax({
                headers: _headers,
                data: JSON.stringify(_json),
                type: 'PATCH',
                url: 'https://api.vtexcrm.com.br/'+_storeName+'/dataentities/CL/documents/',

                success: function(data) {
                    wishlist.methods.userid(_email);
                }
            });
        },

        init : function() {
            this.click();
            this.seleteds();
            this.page();
        },

        init_ajax: function() {

        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    wishlist.mounted();
});

$(document).ajaxStop(function () {
    wishlist.ajax();
});