var Kenner_Busca = {

    methods: {  

        filtersLoading : function(){

        },

        openOrderBy : function(){
            $('.js--orderby-open').click(function(){
                $(this).toggleClass('is--active');
                $('.js--orderby').slideToggle();
            });
        },

        getProductsLength : function(){
            var _value = $('.searchResultsTime .resultado-busca-numero .value').eq(0).text();
            $('.js--products-items').html(_value + ' produtos');
        },

        getSearchedTherm : function(){
            var _search = vtxctx.searchTerm;
            $('.js--search-therm').html(_search);
        },

        init : function() {
            this.openOrderBy();
            this.getProductsLength();
            this.getSearchedTherm();
        },

        init_ajax: function() {
            this.getProductsLength();
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Busca.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Busca.ajax();
});

$(window).load(function(){
    $('.x-departamento-content__filters').addClass('is--loaded');
});