var Kenner_Produto = {

    methods: {  

        productThumbs : function(){
            var _slider = $('.x-product__image .thumbs');
            var _options = {
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                fade: false,
                arrows: false,
                dots: true
            }

            _slider.slick(_options);

            $('.sku-selector-container').on('change', function() {
                $('.x-product__image .thumbs').addClass('is--loading');
                setTimeout(function(){
                    $('.x-product__image .thumbs').removeClass('is--loading');
                    $('.x-product__image .thumbs').removeAttr('class').addClass('thumbs').slick(_options);
                }, 2000)
            });
        },  

        productDescriptionNavigation : function(){
            var _tabs = $('.x-description__tabs--item');
            var _items = $('.x-description__content--item');

            _tabs.eq(0).addClass('is--active');
            _items.eq(0).show();

            _tabs.click(function(){
                var _attr = $(this).attr('data-content');
                if( !$(this).hasClass('is--active') ){
                    _tabs.removeClass('is--active');
                    _items.slideUp();
                    $(this).addClass('is--active');
                    $('#' + _attr).slideDown();
                }else{
                    $(this).removeClass('is--active');
                    $('#' + _attr).next(_items).slideUp();
                }
            });
        },

        slideVitrineNovidades : function(){
            var _vitrines = $('.x-news .x-vitrine__carousel ul');
            var _options = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                arrows: false
            };
            _vitrines.slick(_options);
        },

        createSizeGuideButton : function(){
            $('.x-product__sku .topic.Tamanho').prepend('<button class="js--size-guide-open">Descubra Seu Tamanho</button>');            
        },

        openSizeGuideContainer : function(){
            $(document).on('click', '.js--size-guide-open', function(){
                $('.js--size-guide').fadeIn();
            });

            $('.js--size-guide-close').click(function(){
                $('.js--size-guide').fadeOut();
            });
        },

        navigationSizeGuide : function(){
            var _click = $('.x-size-guide__footer li');
            var _container = $('.x-size-guide__content');

            _click.click(function(event) {
                var _attr = $(this).attr('data-content');
                if( !$(this).hasClass('is--active') ){
                    _click.removeClass('is--active');
                    _container.removeClass('is--active');
                    $(this).addClass('is--active');
                    $('#' + _attr).addClass('is--active');
                }
            });
        },

        activeSizeGuideType : function(){
            var _click = $('.x-size-guide__right--navigation li');
            var _container = $('.x-size-guide__left');

            _container.eq(0).show();

            _click.click(function(event) {
                var _attr = $(this).attr('data-medidas');
                if( !$(this).hasClass('is--active') ){
                    _click.removeClass('is--active');
                    _container.hide();
                    $(this).addClass('is--active');
                    $('#' + _attr).show();
                    Kenner_Produto.methods.checkActiveTable();
                }
            });
        },

        addProductToMinicart : function(){
            var _button = $('.x-product__buy .buy-button-ref');
            var _buttonLink = _button.attr('href');
            if( !_buttonLink.indexOf('alert') ){
                console.log('teste');
                return false;
            }
        },

        getCurrentProduct : function(){
            var _productImage = $('.x-product__image #image-main').attr('src');
            $('.x-product__related ul').prepend('<li class="x-selected"><div class="x-product x-featured"><div class="x-image"><img src='+_productImage+'/></div></div></li>');
        },

        checkRelatedProduts : function(){
            if( !$('.x-product__related ul li').length ){
                $('.x-product__related').hide();
            }
        }, 

        addProductToMinicart : function(){
            var _button = $('.x-product__buy .buy-button-ref');
            var _buttonLink = _button.attr('href');
            _button.click(function(e){
                if( !_buttonLink.indexOf('javascript:alert') != -1  ){

                    var _sku  = $('.buy-button').attr('href').split('sku=')[1].split('&')[0];

                    e.preventDefault();

                    var product = {
                        id: _sku,
                        quantity: 1,
                        seller: 1
                    }

                    return vtexjs.checkout.addToCart([product]).done(function(orderForm) {
                        jQuery.vtex_quick_cart(optionsMiniCart);

                        setTimeout(function(){
                            $('html, body').animate({
                                scrollTop: $("body").offset().top
                            }, 0200);
                            $('body').addClass('is--loading');
                        }, 0200);

                        setTimeout(function(){
                            $('body').removeClass('is--loading');
                        }, 1500);

                        setTimeout(function(){
                            $('.js--minicart-open').parents('li').addClass('is--active');
                            $('.x-minicart').addClass('is--active');
                        }, 1600);

                        setTimeout(function(){
                            $('.js--minicart-open').parents('li').removeClass('is--active');
                            $('.x-minicart').removeClass('is--active');
                        }, 4000);

                    });

                    return false;
                }
            })
        },

        getSizeGuideCM : function(){
            $('.x-size-guide__left--item').each(function(){
                var _element = $(this);
                var _cm = _element.attr('data-cm');
                _element.find('.x-item-svg').append('<span class="x-cm">'+_cm+'</span>');
            });
        },

        removeThumbs : function(){
            $('.x-product__image .thumbs .slick-track > li').each(function(){
                var _element = $(this);
                var _image = _element.find('img').attr('src');
                if( _image.indexOf('THUMB') != -1 ){
                    _element.remove();
                }
            });
        },

        sizeGuideTablesNavigation : function(){
            var _tabs = $('.x-size-guide__right--bottom ul li');
            _tabs.click(function(){
                var _attr = $(this).attr('data-table');
                $('article.js--tabela').hide();
                $('#' + _attr).fadeIn();
            });
        },

        checkActiveTable : function(){
            if( !$( '#tabela-medidas #medidas-masculino' ).is(':visible') ){
                $('.x-size-guide__right--bottom').hide();
            }else{
                $('.x-size-guide__right--bottom').show();
            }
        },

        setSizeLabelWidth: function() {
            if($(".productName:contains('INFANTIL')").length)
                $("label.dimension-Tamanho").css('width', '50px');
        },

        init : function() {
            this.setSizeLabelWidth();
            this.productThumbs();
            this.slideVitrineNovidades();
            this.productDescriptionNavigation();
            this.createSizeGuideButton();
            this.openSizeGuideContainer();
            this.navigationSizeGuide();
            this.activeSizeGuideType();
            this.addProductToMinicart();
            this.getCurrentProduct();
            this.checkRelatedProduts();
            this.getSizeGuideCM();
            this.removeThumbs();
            this.sizeGuideTablesNavigation();
        },

        init_ajax: function() {
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Produto.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Produto.ajax();
});