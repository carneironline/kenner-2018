function scrollTo(elClass) {
    var target_offset;

    target_offset = $(elClass).offset().top;

    $('html, body').animate({scrollTop:target_offset}, 1000);
}

var Kenner_Departamento = {

    methods: {

        mainBannerSlider : function(){
            var _slider = $('.x-departamento-top__banner');
            var _options = {
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                dots: false,
                arrows: false,
            }
            _slider.slick(_options);
        },

        filtersOrderBy : function(){
            $('.js-btn-filter').click(function(){
                $('.x-departamento-content__filters').addClass('is--active');
            });
            $('.x-departamento-content__filters--header-close').click(function(){
                $('.x-departamento-content__filters').removeClass('is--active');
            });

            $('.js-btn-orderby').click(function(){
                $('.x-departamento-content__orderby').addClass('is--active');
            });
            $('.x-departamento-content__orderby--header-close').click(function(){
                $('.x-departamento-content__orderby').removeClass('is--active');
            });
        },

        getProductsLength : function(){
            var _value = $('.searchResultsTime .resultado-busca-numero .value').eq(0).text();
            $('.js--products-items').html(_value + ' produtos');
        },

        closeModal: function() { 
            $('.x-departamento-content__filters').removeClass('is--active');
        },
        closeModalOnSelect: function() {
            var self = this;

            $('.refino label').click(function(){
                scrollTo('.x-departamento-content__buttons-filters');
                self.closeModal();
            });
        }, 

        init : function() {
            this.closeModal();
            this.closeModalOnSelect();
            this.filtersOrderBy();
            this.getProductsLength();
            this.mainBannerSlider();
        },

        init_ajax: function() {
            this.getProductsLength();
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Departamento.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Departamento.ajax();
});

$(window).load(function(){

});