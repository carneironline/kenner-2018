var Kenner_Institucional = {

    methods: {  

        init : function() {
           
        },

        init_ajax: function() {

        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Institucional.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Institucional.ajax();
});