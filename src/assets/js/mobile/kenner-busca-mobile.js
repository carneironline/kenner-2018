var Kenner_Busca = {

    methods: {

        filtersOrderBy : function(){
            $('.js-btn-filter').click(function(){
                $('.x-search-content__filters').addClass('is--active');
            });
            $('.x-search-content__filters--header-close').click(function(){
                $('.x-search-content__filters').removeClass('is--active');
            });

            $('.js-btn-orderby').click(function(){
                $('.x-search-content__orderby').addClass('is--active');
            });
            $('.x-search-content__orderby--header-close').click(function(){
                $('.x-search-content__orderby').removeClass('is--active');
            });
        },

        getProductsLength : function(){
            var _value = $('.searchResultsTime .resultado-busca-numero .value').eq(0).text();
            $('.js--products-items').html(_value + ' produtos');
        },

        getSearchedTherm : function(){
            var _search = vtxctx.searchTerm;
            $('.js--search-therm').html(_search);
        },


        init : function() {
            this.filtersOrderBy();
            this.getProductsLength();
            this.getSearchedTherm();
        },

        init_ajax: function() {
            this.getProductsLength();
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Busca.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Busca.ajax();
});

$(window).load(function(){

});