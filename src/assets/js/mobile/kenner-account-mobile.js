var Kenner_Account = {
    
    data: {
        
    },
    
    methods: {
        
        showContent: function() {
            $('.close, .btn-link').click(function(){
                $('.modal').hide();
            });
            
            $('#edit-data-link').click(function(){
                $('#editar-perfil').show();
                $("html, body").animate({ scrollTop: 0 },300);
                $('#editar-perfil').removeClass('hide');
            });
            
            $('.delete').click(function(){
                $('#address-remove').show();
                $('#address-remove').removeClass('hide');
            });
            
            $('#address-update, .address-update').click(function(){
                $('#address-edit').show();
                $("html, body").animate({ scrollTop: 0 },300);
                $('#address-edit').removeClass('hide');
            });
            
            $('#business-toggle').click(function(){
                $('#business-data').slideToggle();
            });
        },

        getUrl: function(){
            var _location = decodeURIComponent(window.location.pathname.substring(8));
            var _timeLine = $('.x-menu .x-list-menu ul li a');

            $(_timeLine).each(function() {
                var _element = $(this);
                var _destiny = $(_element).attr('href'); 

                if(_location == _destiny){
                    $(_element).addClass('x-active');
                }
            });
        },

        productImageScale : function(){
            setTimeout( function(){
                var _images = $('.x-content .x-orders-list .render-container .myo-order-card .myo-order-product .v-top img, .x-content .x-orders-list .render-container .myo-product-image');
                if( _images.length ){
                    var _imagesSrc = _images.attr('src').replace('50-50', '82-105');
                    _images.attr('src', _imagesSrc);
                }
            }, 600);
        },
        
        init : function() {
            this.showContent();
            this.getUrl();
        },
        
        init_ajax: function() {
            this.productImageScale();
        },
    },
    
    ajax: function() {
        return this.methods.init_ajax();
    },
    
    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Account.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Account.ajax();
});
