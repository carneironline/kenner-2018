var Kopenhagen_Busca_Vazia = {

    methods: { 

        init : function() {
            
        },

        init_ajax: function() {

        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kopenhagen_Busca_Vazia.mounted();
});

$(document).ajaxStop(function () {
    Kopenhagen_Busca_Vazia.ajax();
});