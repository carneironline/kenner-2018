var Kenner_Home = {

    methods: {

        sliderBannerPrincipal : function(){
            var _slider = $('.x-banner .x-banner__list');
            var _options = {
                slidesToShow: 1, 
                slidesToScroll: 1,
                fade: false,
                autoplay: true,
                autoplaySpeed: 3000,
                infinite: true,
                arrows: false,
                dots: true
            };
            _slider.slick(_options);
        },
 
        sliderVitrines : function(){
            var _vitrines = $('.x-vitrine-featured .x-vitrine__carousel ul');
            var _options = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                arrows: true
            };
            _vitrines.slick(_options);
        },

        slideVitrineNovidades : function(){
            var _vitrines = $('.x-news .x-vitrine__carousel ul');
            var _options = {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                arrows: false
            };
            _vitrines.slick(_options);
        },

        init : function() {
            this.sliderBannerPrincipal();
            this.sliderVitrines();
            this.slideVitrineNovidades();
        },

        init_ajax: function() {

        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    Kenner_Home.mounted();
});

$(document).ajaxStop(function () {
    Kenner_Home.ajax();
});